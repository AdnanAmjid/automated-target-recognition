#!/usr/bin/env python

def check_character(result):
    
    """Checks result of OCR analysis
    Args:
        result: the result of OCR analyis
    Returns:
        character: if result matches a
        specified character.
    """
    character ="no match"
    if result == "A":
        print("the character is a ", result)
        character ="A"
          
    elif result == "B":
        print("the character is a ", result)
        character ="B"
        
    elif result == "C":
        print("the character is a ", result)
        character ="C"
        
    elif result == "D":
        print("the character is a ", result)
        character ="D"
        
    elif result == "E":
        print("the character is a ", result)
        character ="E"
            
    elif result == "F":
        print("the character is a ", result)
        character ="F"
            
    elif result == "G":
        print("the character is a ", result)
        character ="G"
        
    elif result == "H":
        print("the character is a ", result)
        character ="H"
       
    elif result == "I":
        print("the character is a ", result)
        character ="I"
        
    elif result == "J":
        print("the character is a ", result)
        character ="J"
        
    elif result == "K":
        print("the character is a ", result)
        character ="K"
        
    elif result == "L":
        print("the character is a ", result)
        character ="L"
        
    elif result == "M": 
        print("the character is a ", result)
        character ="M" 
            
    elif result == "N":             
        print("the character is a ", result)
        character ="N" 
            
    elif result == "O":
        print("the character is a ", result)
        character ="O" 
            
    elif result == "P":
        print("the character is a ", result)
        character ="P" 
            
    elif result == "Q":
        print("the character is a ", result)
        character ="Q" 
        
    elif result == "R":
        print("the character is a ", result)
        character ="R" 
            
    elif result == "S":
        print("the character is a ", result)
        character ="S" 
              
    elif result == "T":
        print("the character is a ", result)
        character ="T"
           
    elif result == "U":
        print("the character is a ", result)
        character ="U"
        
    elif result == "V":               
        print("the character is a ", result)
        character ="V" 
            
    elif result == "W":
        print("the character is a ", result)
        character ="W"
        
    elif result == "X":
        print("the character is a ", result)
        character ="X"
        
    elif result == "Y":
        print("the character is a ", result)
        character ="Y"
        
    elif result == "Z":
        print("the character is a ", result)
        character ="Z"    
        
    return character 
