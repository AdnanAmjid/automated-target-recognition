#!/usr/bin/env python
import cv2
import numpy as np
from scipy.spatial import distance as dist

def order_points(pts):
    
    """orders co-ordinates from contour approximation,
       called by four_point_transform
    Args:
        pts: 4 points of the contour
    Returns:
        nparray: orders co-ordinates topleft to bottomright
    """

    xSorted = pts[np.argsort(pts[:, 0]), :]
    leftMost = xSorted[:2, :]
    rightMost = xSorted[2:, :]
    leftMost = leftMost[np.argsort(leftMost[:, 1]), :]
    (tl, bl) = leftMost
    D = dist.cdist(tl[np.newaxis], rightMost, "euclidean")[0]
    (br, tr) = rightMost[np.argsort(D)[::-1], :]
    return np.array([tl, tr, br, bl], dtype="float32")

def four_point_transform(image, pts):
     
    """calls order points fuunction to order contour points,
       computes new width and height of image and transfroms
       image in relation to new destination matrix
    Args:
        pts: 4 points of the contour
        image :image to perspective transform
    Returns:
        transformed: top down view of image
    """
    #call order points function
    rect = order_points(pts)
    (tl, tr, br, bl) = rect
    #compute height of image
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))
    #compute new height of image
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))
    #construct destination points to create top down view
    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype="float32")
    #apply perespective transform matrix and return transformed image
    transform = cv2.getPerspectiveTransform(rect, dst)
    transformed= cv2.warpPerspective(image, transform, (maxWidth, maxHeight))
    return transformed