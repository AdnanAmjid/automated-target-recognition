#!/usr/bin/env python
import math

    """Predicts GPS-co-ordinates
     Args:
          alt, lat, long, heading, cX, cY
     Returns:
     Predicted GPS lat and long
    """

def predict_gps(alt, heading, long1, lat1,cY,cX):

    # Camera parameters
    cameraAngle = 45
    horizontalRes = 3280
    verticalRes = 2464
    hFOV = 62.2
    vFOV = 48.8
    
    cY = verticalRes-cY # recaclculate, axis are inverted in OpenCV 
    
    # GPS distance alculations
    VDistance2Target = alt*math.tan(math.radians((vFOV/verticalRes * cY) + (cameraAngle-vFOV/2)))
    H = alt/math.cos(math.radians((vFOV/verticalRes * cY) + (cameraAngle-vFOV/2)))
    cX = (cX - (horizontalRes/2)) # check which side of heading- = left side + = right side 
    HDistanceFromTarget = H*math.tan(math.radians(((hFOV/horizontalRes) * cX)))
    OverallDistance2Target = math.sqrt(VDistance2Target**2 + HDistanceFromTarget**2)
    targetAngle = math.degrees(math.atan(HDistanceFromTarget/VDistance2Target))
    bearing = heading+targetAngle
    if bearing > 360:
        bearing = bearing-360
    else:
        bearing = heading+targetAngle

    # Haversine formula convert variables to radians    
    rEarth = 6371 # earths avarage radius km
    epsilon = 0.000001  # threshold for floating-point equality
    rlat1 = math.radians(lat1)
    rlon1 = math.radians(long1)
    rbearing = math.radians(bearing)
    rdistance = (OverallDistance2Target/1000)/rEarth #convert distance to km
    
    # Haversine formula 
    rlat2 = math.asin(math.sin(rlat1) * math.cos(rdistance) + math.cos(rlat1) * math.sin(rdistance) * math.cos(rbearing))

    if math.cos(rlat2) == 0 or abs(math.cos(rlat2)) < epsilon: # Endpoint a pole
                rlon2 = rlon1
    else:
                rlon2 = rlon1 + math.atan2(math.sin(rbearing) * math.sin(rdistance) * math.cos(rlat1),
                                           math.cos(rdistance) - math.sin(rlat1) * math.sin(rlat2))

    lat2 = math.degrees(rlat2)
    long2 = math.degrees(rlon2)
    predictedGPS=("lat", lat2, "long", long2)
    return predictedGPS
