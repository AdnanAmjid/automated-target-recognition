#!/usr/bin/env python
import cv2

def red_threshold(hsv):
    
    """Thresholds HSV image
    Args:
        hsv: HSV image
    Returns:
        mask: black & white mask based on threshold values
    """
    mask1 = cv2.inRange(hsv, (0, 150, 200), (5, 255, 255))  # upper&lower lims 1st mask
    mask2 = cv2.inRange(hsv, (140, 65, 70), (180, 255, 255))# upper&lower lims 2nd mask
    mask = cv2.bitwise_or(mask1, mask2) #combine masks
    return mask
