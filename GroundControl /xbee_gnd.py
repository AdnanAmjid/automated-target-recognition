#!/usr/bin/env python
import serial

"""
Reads data sent from UAV using serial port
Writes data recived to a file 
"""
#  define serial port
ser = serial.Serial ('COM10', 57600)
# create file to log results 
file = open("C:/Users/Adnan/Documents/Final Year Project/UASresults.txt", "a")
while True:
    x = ser.read_until(b')')# read serial port
    x = (x.decode('utf-8')) # decode contents 
    print(x) #print to console 
    
    file.write(x+'\n') # write to created file
    file.flush() # clear file buffer
    ser.flush() # clear serial buffer
    
    
ser.close() # close serial port
file.close # close file 