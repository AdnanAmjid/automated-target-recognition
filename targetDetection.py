#!/usr/bin/env python
#libraries imported
import cv2
import imutils
import pytesseract
import math
import multiprocessing 
import queue
import serial
import time
from pymavlink import mavutil
from picamera.array import PiRGBArray
from picamera import PiCamera
from imutils.video import VideoStream
# my custom modules
from Modules.threshold import red_threshold
from Modules.ptransform import *
from Modules.ocr import check_character
from Modules.gps import predict_gps

#serial port for xbee
ser=serial.Serial(
   port='/dev/ttyUSB0',
   baudrate = 57600,
)

#establish connection to pixhawk 
master = mavutil.mavlink_connection('/dev/ttyAMA0', baud = 57600)
print("Established Connection")

#camera settings 
camera = PiCamera()
camera.resolution = (3280,2464)
camera.framerate = 12
camera.exposure_mode ='auto' 
#camera.shutter_speed = 15000
#camera.iso = 1600
rawCapture = PiRGBArray(camera, size=(3280,2464)) 
time.sleep(3) #warmup camera


#continously grab camera image in bgr colour-space
for frame in camera.capture_continuous(rawCapture, format="bgr"):
    image=frame.array #get image from stream 
    rawCapture.truncate(0) #clear stream
    detected=0 #nothing initialy detected
    
    #convert colours space & threshold
    #image = cv2.resize(image, (600, 600))# resize only when debugging
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    mask = red_threshold(hsv)
    mask = cv2.GaussianBlur(mask, (5, 5), 0)
 
   
    #find contours in mask 
    (_, contours, _) = cv2.findContours(mask, cv2.RETR_EXTERNAL,
                                        cv2.CHAIN_APPROX_SIMPLE)
    
    print("Found %d objects." % len(contours))
    #c = sorted(contours, key=cv2.contourArea, reverse=True)[:10]
    
    for c in contours: 
        c = max(contours, key=cv2.contourArea)
        
        #approximate contour
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.02 * peri, True) 
        area = cv2.contourArea(c)
        
        #ensure contour has 4 sides & > min area
        if len(approx) == 4 and area > 400:
            hullarea =cv2.contourArea(cv2.convexHull(c))
            solidity = area/float(hullarea)
            if solidity > 0.9:
                detected=1
                break
        else:
            print("does not meet critera")
            break
    
    if detected == 1:
        print("target detected")
        
        #compute contour centre, get co-ordinates
        M = cv2.moments(c)
        cX = int(M["m10"] / M["m00"])
        cY = int(M["m01"] / M["m00"])
       
        #perspective transform on contour region
        transformed = four_point_transform(mask, approx.reshape(4, 2))
        ROI = cv2.resize(transformed, (100, 100))
        ROI = ROI[9:92, 9:91]#crop
         
        
        
        def OCR(ROI, angle):
            """runs OCR on 4 rotated ROI
            Args:
                ROI & rotation angle
            Returns:
            character: result is checked using check_character
            function
            """
            config = (' --psm 10') #treat ROI as single character
            rotated = imutils.rotate_bound(ROI, angle) #rotate ROI 90,180, 270
            result = (pytesseract.image_to_string(rotated, config=config, lang='eng'))# run OCR on rotated ROI
            character= check_character(result) #function checks result matches specified character
            queue.put(character)# put charecter into queue
            
        def GPS_TELEM(cY,cX):
            """get gps data from pixhawk
            Args:
                target cy,cx co-ordinates
            Returns:
            GPS co-ordinates: target co-ordiantes & telem data
            used as arguments for predict_gps function which calcs & returns GPS
            """
            msg1 = master.recv_match(type='VFR_HUD', blocking=True)#heading
            msg2 = master.recv_match(type='GPS_RAW_INT', blocking=True)#gps
            msg3 = master.recv_match(type='ALTITUDE', blocking = True)#relative alt
            alt=(master.messages['ALTITUDE'].altitude_relative)
            lat1 = (master.messages ['GPS_RAW_INT'].lat)/10000000
            long1 =  (master.messages['GPS_RAW_INT'].lon)/10000000
            heading=(master.messages['VFR_HUD'].heading)
            cY=cY
            cX=cX
            GPS = predict_gps(alt, heading, long1, lat1,cY,cX) #function to predict GPS
            queue2.put(GPS) #put GPS into second queue
    
        if __name__=="__main__":
            #create queues
            queue = multiprocessing.Queue()  #OCR process queue
            queue2 = multiprocessing.Queue() #GPS process queue
            #define each process, target function and function arguments
            ocr1 = multiprocessing.Process(target=OCR, args=(ROI,0))
            ocr2 = multiprocessing.Process(target=OCR, args=(ROI,90))
            ocr3 = multiprocessing.Process(target=OCR, args=(ROI,180))
            ocr4 = multiprocessing.Process(target=OCR, args=(ROI,270))
            GPS = multiprocessing.Process(target=GPS_TELEM, args=(cX,cY))
            #start each process
            ocr1.start()
            ocr2.start()
            ocr3.start()
            ocr4.start()
            GPS.start()
            #get results from each process
            ocr1=queue.get()
            ocr2=queue.get()
            ocr3=queue.get()
            ocr4=queue.get()
            GPS=queue2.get()
               
            #ensure character is returned from each process
            #if nothing has been detected, go to start
            if ocr1 != "no match":
                letter = ocr1
            elif ocr2 != "no match":   
                letter = ocr2
            elif ocr3 != "no match":   
                letter = ocr3
            elif ocr4 != "no match":   
                letter = ocr4
            elif ocr1 or ocr2 or ocr3 or ocr4 == "no match":
                letter = 0
                detected=0
    
            #convert results to string & write to serial port            
            string = (letter,GPS)
            x=str(string)
            #print(string)
            ser.write(str.encode(x))
            ser.flush()

    


